import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap } from 'rxjs/operators';

import { MovieService } from '../services/movie-search.service';
import { IMovieDetails } from '../models/movie-details.model';

@Component({
  selector: 'app-movie-details',
  providers: [MovieService],
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.sass']
})
export class MovieDetailsComponent implements OnInit {
  id: string;
  movie: IMovieDetails;

  constructor(private route: ActivatedRoute, private movieService: MovieService) { }

  ngOnInit(): void {
    this.route.paramMap.pipe(
      switchMap(params => params.getAll('id'))
    )
      .subscribe(
        data => this.id = data
      )

    this.movieService.fetchMovieByID(this.id)
      .subscribe((result) => {
        this.movie = result;
      })
  }

}
