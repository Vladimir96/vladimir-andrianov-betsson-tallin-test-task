import { ISearchResult, IMovie } from '../../models/movies.model';

export class fetchMovies {
    static readonly type = '[Movies] FetchAll'
    constructor(
        public payload: IMovie[],
        public searchText: string,
        public totalResults: number,
        public Response: string,
        public currentPage: number,
    ) { }
}
