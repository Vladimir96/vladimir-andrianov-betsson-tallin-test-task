import { State, Action, StateContext, Selector } from '@ngxs/store';

import { ISearchResult, IMovie } from '../../models/movies.model';
import { fetchMovies } from '../actions/movies.actions';

export class MoviesStateModel {
    movies: IMovie[];
    searchText: string;
    totalResults: number;
    response: string;
    currentPage: number;
}

@State<MoviesStateModel>({
    name: 'movies',
    defaults: {
        movies: undefined,
        searchText: '',
        totalResults: 0,
        response: 'True',
        currentPage: 1,
    }
})

export class MoviesState {

    @Selector()
    static getMovies(state: MoviesStateModel) {
        return state.movies
    }

    @Selector()
    static getSearchText(state: MoviesStateModel) {
        return state.searchText
    }

    @Selector()
    static getTotalResults(state: MoviesStateModel) {
        return state.totalResults
    }

    @Selector()
    static getResponse(state: MoviesStateModel) {
        return state.response
    }

    @Selector()
    static getCurrentPage(state: MoviesStateModel) {
        return state.currentPage
    }


    @Action(fetchMovies)
    add({ getState, setState }: StateContext<MoviesStateModel>, { payload, searchText, totalResults, Response, currentPage }: fetchMovies) {
        const state = getState();
        setState((state) => ({ ...state, movies: payload, searchText: searchText, totalResults: totalResults, response: Response, currentPage: currentPage }));
    }
}
