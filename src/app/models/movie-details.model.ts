export interface IMovieDetails {
    Title: string,
    Year: string,
    Type: string,
    Poster: string,
    Plot: string,
    imdbRating: string,
    Runtime: string,
    Genre: string
}
