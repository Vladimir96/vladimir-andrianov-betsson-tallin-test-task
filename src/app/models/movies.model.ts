export interface IMovie {
    Title: string,
    Year: string,
    Type: string,
    Poster: string,
    imdbID: string,
}

export interface ISearchResult {
    movies: IMovie[],
    totalResults: number,
    Response: string,
}
