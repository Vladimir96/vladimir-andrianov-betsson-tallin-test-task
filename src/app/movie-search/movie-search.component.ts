import { Component, OnInit, HostListener, OnChanges } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { Observable } from 'rxjs/internal/Observable';

import { MovieService } from '../services/movie-search.service';
import { fetchMovies } from '../store/actions/movies.actions';
import { MoviesState } from '../store/state/movies.state';
import { IMovie } from '../models/movies.model';

@Component({
  selector: 'app-movie-search',
  providers: [MovieService],
  templateUrl: './movie-search.component.html',
  styleUrls: ['./movie-search.component.sass']
})
export class MovieSearchComponent implements OnInit {
  @HostListener('window:scroll') onScroll() {
    if (window.innerHeight + window.scrollY >= document.documentElement.scrollHeight - 10) {
      if (this.totalResults > this.currentPage && this.response == 'True') {
        this.spinner = true;
        this.currentPage += 1;
        this.addMoviesOnScroll();
      }
    }
  }

  spinner: boolean;
  movies: IMovie[];
  searchText: string;
  totalResults: number;
  response: string;
  currentPage: number;

  @Select(MoviesState.getMovies) moviesStored: Observable<IMovie[]>;
  @Select(MoviesState.getSearchText) searchTextStored: Observable<string>;
  @Select(MoviesState.getTotalResults) totalResultsStored: Observable<number>;
  @Select(MoviesState.getResponse) responseStored: Observable<string>;
  @Select(MoviesState.getCurrentPage) currentPageStored: Observable<number>;


  addMoviesOnScroll() {
    this.movieService.fetchMovieByTitle(this.searchText, this.currentPage)
      .subscribe((result) => {
        if (result.Response == "False") {
          this.store.dispatch(new fetchMovies(this.movies, this.searchText, result.totalResults, result.Response, this.currentPage));
        } else {
          this.movies = this.movies.concat(result['Search']);
          this.store.dispatch(new fetchMovies(this.movies, this.searchText, result.totalResults, result.Response, this.currentPage));
        }
      });
    setTimeout(() => { this.spinner = false; }, 1000)
  }

  searchMovieOnButton(title: string) {
    this.spinner = false;
    this.currentPage = 1;
    this.movieService.fetchMovieByTitle(title, this.currentPage)
      .subscribe((result) => {
        this.store.dispatch(new fetchMovies(result['Search'], title, result.totalResults, result.Response, this.currentPage));
      });
  }

  ngOnInit() {
    this.totalResults = 0;
    this.moviesStored.subscribe((movies) => { this.movies = movies })
    this.searchTextStored.subscribe((title) => { this.searchText = title })
    this.totalResultsStored.subscribe((total) => { this.totalResults = total })
    this.responseStored.subscribe((response) => { this.response = response })
    this.currentPageStored.subscribe((page) => { this.currentPage = page })
  }

  constructor(private movieService: MovieService, private store: Store) { }
}
