import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/internal/Observable';

import { ISearchResult } from '../models/movies.model';
import { IMovieDetails } from '../models/movie-details.model';
import { environment } from '../../environments/environment.prod';

@Injectable()
export class MovieService {

    constructor(private http: HttpClient) { }

    fetchMovieByTitle(title: string, page: number): Observable<ISearchResult> {
        const url = 'http://www.omdbapi.com/?apikey=' + environment.API_KEY + '&s=' + title + '&page=' + page;
        return this.http.get<ISearchResult>(url);
    }

    fetchMovieByID(id: string): Observable<IMovieDetails> {
        const url = 'http://www.omdbapi.com/?apikey=' + environment.API_KEY + '&i=' + id;
        return this.http.get<IMovieDetails>(url);
    }
}
