import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { MovieService } from './movie-search.service';
import { environment } from '../../environments/environment.prod';

describe('Fetching movies', () => {
    let httpTestingController: HttpTestingController;
    let service: MovieService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [MovieService],
            imports: [HttpClientTestingModule]
        });

        // We inject our service (which imports the HttpClient) and the Test Controller
        httpTestingController = TestBed.get(HttpTestingController);
        service = TestBed.get(MovieService);
    });

    it('Should return Observable with movies found by title matching the correct data', () => {
        const mockMovies = [
            {
                Title: "Funny People",
                Year: "2009",
                Type: "movie",
                Poster: "https://pictureServer.com/somePic1.jpg",
                imdbID: "tt1201167"
            },
            {
                Title: "The People vs. Larry Flynt",
                Year: "1996",
                Type: "movie",
                Poster: "https://pictureServer.com/somePic2.jpg",
                imdbID: "tt0117318"
            }
        ];

        let page = 1;
        service.fetchMovieByTitle("People", page)
            .subscribe(moviesData => {
                expect(moviesData[0].Title).toEqual('Funny People');
                expect(moviesData[0].Year).toEqual('2009');
                expect(moviesData[0].Type).toEqual('movie');
                expect(moviesData[0].Poster).toEqual('https://pictureServer.com/somePic1.jpg');
                expect(moviesData[0].imdbID).toEqual('tt1201167');

                expect(moviesData[1].Title).toEqual('The People vs. Larry Flynt');
                expect(moviesData[1].Year).toEqual('1996');
                expect(moviesData[1].Type).toEqual('movie');
                expect(moviesData[1].Poster).toEqual('https://pictureServer.com/somePic2.jpg');
                expect(moviesData[1].imdbID).toEqual('tt0117318');
            });

        const req = httpTestingController.expectOne(
            'http://www.omdbapi.com/?apikey=' + environment.API_KEY + '&s=People' + '&page=' + page
        );

        req.flush(mockMovies);
    });

    it('Should return Observable with the movie details matching the correct data', () => {
        const mockMovieDetails = [
            {
                Title: "Funny People",
                Year: "2009",
                Type: "movie",
                Poster: "https://pictureServer.com/somePic1.jpg",
                Plot: "When seasoned comedian George...",
                imdbRating: "6.3",
                Runtime: "146 min",
                Genre: "Comedy, Drama"
            }
        ];

        service.fetchMovieByID("tt1201167")
            .subscribe(movieDetails => {
                expect(movieDetails[0].Title).toEqual('Funny People');
                expect(movieDetails[0].Year).toEqual('2009');
                expect(movieDetails[0].Type).toEqual('movie');
                expect(movieDetails[0].Poster).toEqual('https://pictureServer.com/somePic1.jpg');
                expect(movieDetails[0].Plot).toEqual('When seasoned comedian George...');
                expect(movieDetails[0].imdbRating).toEqual('6.3');
                expect(movieDetails[0].Runtime).toEqual('146 min');
                expect(movieDetails[0].Genre).toEqual('Comedy, Drama');
            });

        const req = httpTestingController.expectOne(
            'http://www.omdbapi.com/?apikey=' + environment.API_KEY + '&i=tt1201167'
        );

        req.flush(mockMovieDetails);
    });
});
