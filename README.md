# Test Task made by Vladimir Andrianov for the Fullstack position at Betsson Group Tallinn.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.9.
NGXS is used for the state management.
The app has movie search list animated progress bar on scrolling to the bottom and progress spinner in the movie details page. 

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
